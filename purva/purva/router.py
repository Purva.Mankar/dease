from apiproject.viewsets import UserViewset
from rest_framework import routers

router = routers.DefaultRouter()
router.register('user',UserViewset)

#localhost:/p/api/user/3
#GET,POST,UPDATE,DELETE
# LIST, RETRIEVE-get
