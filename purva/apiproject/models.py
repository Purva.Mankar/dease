from django.db import models

# Create your models here.
class User(models.Model):
    fullname = models.CharField(max_length=100)
    user_id = models.CharField(max_length=3)
    mobile = models.CharField(max_length=15)
    user_mail = models.EmailField(max_length=100)
    password = models.CharField(max_length=50)
    
    #create / insert /add- POST
    #retrieve /fetch-GET
    #update / edit-using PK -PUT
    #delete / remove-DELETE
    